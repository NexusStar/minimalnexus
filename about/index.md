---
layout: article
title: About me
date: 2016-01-06T21:29:58+02:00
modified:
excerpt:
tags: []
image:
  feature:
---

# About me title


Not to get confused if you care NexusStar was not my real name.

My name is Petar Nikov I am father, designer, developer, coder.

## What I do that passes for work hours

Most of my daily hours I spend designing stuff and codding stuff.

Designing mostly traditional print materials, and when needed I do 
and have done also web-design.

I consider myself pretty good front-end developer and Drupal enthusiast.

While I love my daily job I really love programming.

## What I do for fun

I love to learn new things, listening to audio books and reading.

Tweeking my working environment.

And one day just to windsurf all day long without rushing and care.

