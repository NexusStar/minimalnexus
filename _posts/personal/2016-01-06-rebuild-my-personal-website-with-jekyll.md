---
layout: article
title: Rebuild My Personal Website With Jekyll
modified:
categories: personal
excerpt: Change my website from Drupal to Jekyll
tags: [jekyll, drupal, blog]
image:
  feature:
date: 2016-01-06T22:04:06+02:00
comments: true
---

##Why

My personal website went to too many changes.
I start with simple static html and JS and later went on hunt for ultimate CMS
start with Joomla went to WordPress and hated it from the start
so ended in Drupal camp for good.

So after I choose to use Drupal for website creation the next reasonable step for me was to made my
personal website also with this wonderful framework.

I went to more than few iteration to my web site with it.

##But

There is always big _but_ with every choice.

Drupal is great but it is quite a overkill for personal web site.
It needs modules,configuration etc.

And in the last 4 years I tend to lean mostly on the front-end development.
All that bring me back to static web-site and Jekyll

* I wanted to have cool design
* I wanted it to be simple to maintain
* I wanted it to be easy changable
* I wanted it to use markdown so to write every post in VIM

So here is my new blog and personal page using Jekyll
there are more than few bumps on the road to be satisfied.
_but_ I am never satisfied.

__Never gona stop learning__
