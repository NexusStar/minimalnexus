# NexusStar personal web-site and blog

This is my personal blog it is an itch of mine to remake it from time to time.
So it's never in finished state. It's more a playground for me.

## Fresh install on local machine

Clone code and run 
```
$ npm install
``
```
$ bundle install
```

## Jekyll, Gulp and company

Current iteration is made with Jekyll and [Skinny bones theme](https://github.com/mmistakes/skinny-bones-jekyll/tree/jekyll3).

I made some changes:

	* Added automation with Gulp
	* Remove icons from social buttons

And am planning to made some more:

	* Remove images except in portfolio pages.

### Simple deployment
[simple deployment](https://bitbucket.org/lilliputten/automatic-bitbucket-deploy/overview)

### Deployment problems

First of all make sure that the script works and if it works what part of it works and what don't

## Create blog posts and update existing ones
