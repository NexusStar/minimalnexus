var gulp        = require('gulp');
var plugin      = require('gulp-load-plugins')();
var fs          = require('fs');
var del         = require('del');
var cp          = require('child_process');
var concat      = require('gulp-concat');
var browserSync = require('browser-sync');
var browserify  = require('browserify');

var messages = {
  jekyllBuild: '<span style="color: grey">Running:</span> $ jekyll build'
};

//handle errors function needs emit end
function handleError(err) {
    console.log(err.toString());
      this.emit('end');
};

/**
 * Build the Jekyll Site
 */
gulp.task('jekyll:build', function (done) {
  browserSync.notify(messages.jekyllBuild);
  return cp.spawn('bundle', ['exec','jekyll','build','--incremental'], {stdio: 'inherit'})
  .on('close', done);
});

/**
 * Rebuild Jekyll & do page reload
 */
gulp.task('jekyll:rebuild', ['jekyll:build'], function () {
  browserSync.reload({});
});

/**
 * Wait for jekyll-build, then launch the Server
 */
gulp.task('browser:sync', ['sass','uglify', 'jekyll:build'], function() {
  browserSync({
    server: {
      baseDir: '_site'
    }
  });
});

/**
 * This task generates CSS from all SCSS files.
 */
gulp.task('sass', function () {
  return gulp.src('./_scss/**/*.scss')
  .pipe(plugin.sourcemaps.init())
  .pipe(plugin.sass({
    noCache: true,
    outputStyle: "compressed",
    lineNumbers: false,
    loadPath: './assets/css/*',
    sourceMap: true
  }).on("error", handleError))
  .pipe(plugin.sourcemaps.write('./maps'))
  .pipe(gulp.dest('./assets/css'))
  .pipe(plugin.notify({
    title: "SASS Compiled",
    message: "All SASS recompiled to CSS",
    onLast: true
  }));
});

/*
 * This task make css analytics
 * */
gulp.task('parker', function() {
  return gulp.src('./assets/css/*.css')
  .pipe(plugin.parker({
    file: 'CSSreport.md',
    title: 'CSS test report',
    metrics: [
      'TotalStylesheets',
      'TotalStylesheetSize',
      'TotalRules',
      'TotalSelectors',
      'TotalIdentifiers',
      'TotalDeclarations',
      'SelectorsPerRule',
      'IdentifiersPerSelector',
      'SpecificityPerSelector',
      'TopSelectorSpecificity',
      'TopSelectorSpecificitySelector',
      'TotalIdSelectors',
      'TotalUniqueColours',
      'TotalImportantKeywords',
      'TotalMediaQueries'
    ]}));
});

/**
 * This task minifies Javascript from the /_js folder and places them in assets/js
 */
gulp.task('uglify', function() {
  return gulp.src([
    './_js/plugins/jquery.visible.js',
    './_js/_main.js'
  ])
  .pipe(plugin.sourcemaps.init())
  .pipe(concat('scripts.min.js'))
  .pipe(plugin.uglify())
  .pipe(plugin.sourcemaps.write('./maps'))
  .pipe(gulp.dest('./assets/js'))
  .on('error', handleError)
  .pipe(plugin.notify({
    title: "JS Minified",
    message: "All JS minified",
    onLast: true
  }))
});

/**
 * Task when scss files are changed: recompress the css and rebuild
 * then when those are done reload the browser.
 */
gulp.task('css:reload', ['sass', 'parker', 'jekyll:build'], function() {
  browserSync.reload({});
});

// Task when js files are changed: ensures the js `uglify` task is complete before
// reloading browsers
gulp.task('js:reload', ['uglify', 'jekyll:build'],function(){
  browserSync.reload({});
});

/**
 * Copy js vendor files not bundled inside scripts.min.js
 */
gulp.task('js:copy', function(){
  gulp.src('./node_modules/jquery/dist/jquery.min.js')
  .pipe(gulp.dest('./assets/js/vendor'))
});

/**
 * Defines the watcher task.
 */
gulp.task('watch', function() {
  debugger;
  //watches scss files and reloads browsers
  gulp.watch('_scss/**/*.scss', ['css:reload']);

  //watches js files and reloads browser
  gulp.watch('./_js/**/*js', ['js:reload']);

  //watches html and md files and rebuilds
  gulp.watch(['*.md', '_layouts/*.html','_includes/*.html', '_posts/**/*','portfolio/*','about/*', '_data/*'], ['jekyll:rebuild']);
});

gulp.task('default', ['watch', 'browser:sync']);
